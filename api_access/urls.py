from django.urls import path

from .views import order_list

app_name = 'api_access'


urlpatterns = [
    path(r'get-order-list/', order_list, name='order_list'),
]
