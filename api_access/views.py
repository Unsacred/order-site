import json

from django.conf import settings
from django.http import JsonResponse, HttpResponse

from http_basic_auth import parse_header

from order.models import Order


def order_list(request):
    if 'Authorization' not in request.META:
        return HttpResponse('Unauthorized', status=401)

    authorization = request.META['Authorization']
    user, password = parse_header(authorization)

    if user not in settings.HTTP_BASIC_AUTH:
        return HttpResponse('Unauthorized', status=401)

    if settings.HTTP_BASIC_AUTH[user] != password:
        return HttpResponse('Unauthorized', status=401)

    orders = [o.to_json() for o in Order.objects.all()]
    return JsonResponse(json.dumps(orders), safe=False)
