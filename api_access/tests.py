import json

from http_basic_auth import generate_token
from django.test import TestCase

from customer.models import Customer
from order.models import Order
from user.models import User


class TestBasicAuth(TestCase):
    def test_ok_auth(self):
        response = self.client.get(
            path='/api/get-order-list/',
            Authorization=f'Basic {generate_token("user", "qwe")}'
        )
        self.assertEqual(response.status_code, 200)

    def test_order_is_ok(self):
        user = User.objects.create_user(username='bob')
        customer = Customer.objects.create(name='OOO')
        order = Order.objects.create(user=user, customer=customer)
        order_dict = order.to_json()
        response = self.client.get(
            path='/api/get-order-list/',
            Authorization=f'Basic {generate_token("user", "qwe")}'
        )
        order_list = json.loads(response.json())
        self.assertEqual(order_dict, order_list[0])
