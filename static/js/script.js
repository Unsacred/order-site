document.onreadystatechange =  function () {
    if (document.readyState == "complete") {
        const button =  document.querySelector('.order_create_butt');
        button.addEventListener('click', function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            const data = new FormData(document.querySelector('.order_create_form'));

            fetch('/order/create.json', {method: 'POST', body: data})
            .then((response) => {
                if (!response.ok) {
                    return console.error('Не удалось получить response')
                }
                response.json().then((resp)=>{
                    console.log(resp);
                    const order = document.createElement('li');
                    const price = Intl.NumberFormat('en', {style: 'decimal', minimumFractionDigits: 2})
                    .format(parseFloat(resp.price));
                    order.textContent = 'Исполнитель: ' + resp.user + ', дата создания: ' + resp.date + ', заказчик: '
                    + resp.customer.name + ', цена: ' + price + '';
                    const order_list = document.querySelector('.order_list')
                    const link = document.createElement('a');
                    link.setAttribute('href', '/order/delete/'+resp.id)
                    link.textContent= 'Удалить заказ'
                    link.classList.add('btn', 'btn-primary')
                    order.append(link)
                    order_list.append(order)

                });
            });
        });

        fetch('/user/home.json')
        .then((response) => {
            const resp = response.json();
        });
    }
}
