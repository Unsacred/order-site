from django.db import models


class Customer(models.Model):
    name = models.CharField(unique=True, verbose_name='Заказчик', max_length=50)

    def __str__(self):
        return self.name

    def to_json(self):
        return {
            "id": int(self.id),
            "name": str(self.name)
        }
