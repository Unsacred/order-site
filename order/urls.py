from django.urls import path

from .views import CreateOrderView, orderstat_by_month, DeleteOrderView

app_name = 'order'


urlpatterns = [
    path(r'create/', CreateOrderView.as_view(), name='create'),
    path(r'create.json', CreateOrderView.as_view(), kwargs={'is_json': True}, name='create_json'),
    path(r'', orderstat_by_month, name='list'),
    path(r'delete/<int:pk>', DeleteOrderView.as_view(), name='delete')
]