from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.views.generic import FormView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import OrderForm
from .models import Order

from collections import defaultdict
from datetime import date


class CreateOrderView(LoginRequiredMixin, FormView):
    form_class = OrderForm
    success_url = '/user/home'

    def form_valid(self, form):
        order = form.save(commit=False)
        order.user = self.request.user
        order.save()

        if self.kwargs.get('is_json'):
            return JsonResponse(order.to_json())

        return redirect(self.request.user.get_home_url())


def orderstat_by_month(request):
    orders = Order.objects.filter(date__month=date.today().month).order_by('date')
    by_weeks = defaultdict(list)
    data =[]

    for order in orders:
        by_weeks[order.date.isocalendar()[1]].append(order)
    weeks = by_weeks.values()

    for week in weeks:
        by_day = defaultdict(list)
        for order in week:
            by_day[order.date.date()].append(order)
        data_by_day = {}
        for key, value in by_day.items():
            data_by_day[key] = get_stat(value)
        data.append(data_by_day)

    return render(request, 'order/order_stat_list_month.html', {'order_stat_by_weeks': data})


def get_stat(orders):
    end_price = 0
    users = set()

    for order in orders:
        users.add(order.user)
        end_price += order.price

    return {'users': users, 'end_price': end_price}


class DeleteOrderView(LoginRequiredMixin, DeleteView):
    model = Order
    success_url = '/user/home'
