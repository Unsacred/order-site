from django.db import models

from user.models import User
from customer.models import Customer


class Order(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        related_name='orders',
        verbose_name='исполнитель'
    )
    date = models.DateTimeField(auto_now_add=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, verbose_name='Заказчик')
    price = models.DecimalField(verbose_name='Цена', decimal_places=2, max_digits=8, default=0)

    def to_json(self):
        return {
            "id": str(self.id),
            "user": str(self.user),
            "date": str(self.date),
            "customer": self.customer.to_json(),
            "price": float(self.price)
        }

    def __str__(self):
        return f'Исполнитель: {self.user}, дата создания: {self.date}, заказчик: {self.customer}, цена: {self.price}'
