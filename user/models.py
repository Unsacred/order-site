from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import validate_slug
from django.urls import reverse_lazy


class User(AbstractUser):
    username = models.CharField(
        verbose_name="Имя пользоваталя",
        max_length=25,
        unique=True,
        validators=[validate_slug],
        error_messages={
            "unique": "К сожалению пользователь с таким имененем уже существует",
        },
    )

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse_lazy('users:detail', args=[self.username])

    def get_home_url(self):
        return reverse_lazy('users:home')
