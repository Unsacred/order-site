from django.urls import path

from .views import UserDetailView, UserLoginView, UserLogoutView, home_user_page, UserHomeJsonView

app_name = 'users'


urlpatterns = [
    path(r'login/', UserLoginView.as_view(), name='login'),
    path(r'detail/<slug:slug>', UserDetailView.as_view(), name='detail'),
    path(r'logout/', UserLogoutView.as_view(), name='logout'),
    path(r'home/', home_user_page, name='home'),
    path(r'home.json', UserHomeJsonView.as_view(), name='home_json'),
]
