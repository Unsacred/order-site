from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView
from django.http import JsonResponse
from django.shortcuts import reverse, HttpResponseRedirect, render
from django.views.generic import DetailView, View

from user.models import User
from order.models import Order
from order.forms import OrderForm


class UserDetailView(DetailView):
    model = User
    slug_field = "username"

    def get_context_data(self, **kwargs):
        ctx = super(UserDetailView, self).get_context_data(**kwargs)
        user = self.request.user
        ctx['orders'] = Order.objects.filter(user=user)
        return ctx


class UserLoginView(LoginView):
    template_name = 'user/user_login.html'


class UserLogoutView(LogoutView):

    def get(self,*args, **kwargs):
        logout(self.request)
        return HttpResponseRedirect(reverse('index'))


@login_required
def home_user_page(request):
    return render(request, 'user/user_home.html', {'orders': Order.objects.filter(user=request.user),
                                                   'form': OrderForm(),
                                                   })


class UserHomeJsonView(View):
    http_method_names = ['get']

    def get(self, request):
        return JsonResponse({'orders': [o.to_json() for o in Order.objects.filter(user=request.user)]})
